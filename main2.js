var person = (function (){
    var details = {
        firstName: 'Krzysztof',
        lastName: 'Kotara',
    },
    calculateBalance = function(){
        var sum=0;
        for( i =0; i < this.accountslist.length; i++) {
            sum += this.accountslist[i].balance;
        }
        return sum;
    };
    
    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountslist: [{
            balance: 5,
            currency: 'CHF'
        }],
        addAcount: function(account){
            this.accountslist.push(account);
        },
        sayHello: function(){
            return this.firstName + ' ' + this.lastName + ' ' + calculateBalance.call(this);    
        }
    };
})();
console.log(person.sayHello());
person.addAcount({
    balance: 15,
    currency: 'PLN'
});
console.log(person.sayHello());