var Account = function (balance, currency){
    this.balance = balance;
    this.currency = currency;
};

var person = (function (){
    var details = {
        firstName: 'Krzysztof',
        lastName: 'Kotara',
    },
    calculateBalance = function(){
        var sum=0;
        for( i =0; i < person.accountslist.length; i++) {
            sum += person.accountslist[i].balance;
        }
        return sum;
    };
    
    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountslist: [{
            balance: 5,
            currency: 'CHF'
        }],
        addAcount: function(account){
            person.accountslist.push(account);
        },
        sayHello: function(){
            return this.firstName + ' ' + this.lastName + ' ' + calculateBalance.call(this);    
        }
    };
})();
console.log(person.sayHello());
person.addAcount({
    balance: 15,
    currency: 'PLN'
});
console.log(person.sayHello());
person.accountslist[0] = new Account(999,'USS');
console.log(person.accountslist[0].balance +' ' + person.accountslist[0].currency);
console.log(person.sayHello());
