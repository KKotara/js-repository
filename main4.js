class Account { 
    constructor (balance, currency){
        this.balance = balance;
        this.currency = currency;
    }
};

class Person {
    constructor (firstName, lastName, accountslist){
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountslist = accountslist; 
    }
    calculateBalance(){
        var sum=0;
        for( let stan of this.accountslist) {
            sum += stan.balance;
        }
        return sum;
    }
    addAcount(account){
        this.accountslist.push(account);
    };
    filterPositiveAccounts(){
        return this.accountslist.filter(function(konto){
            return konto.balance > 0;
        });
    }
    sayHello(){
        return this.firstName + ' ' + this.lastName + ' ' + this.calculateBalance();    
    }
};

const ja = new Person ('Krzysztof','Kotara',[new Account(9999,'JPN'),new Account(1,'JPN'),new Account(-1,'EUR')]);
console.log(ja.sayHello());
ja.addAcount(new Account(-10,'JPN'));
console.log(ja.filterPositiveAccounts());
console.log(ja.sayHello());
