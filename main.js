var personFactory = function (){
    var details = {
        firstName: 'Krzysztof',
        lastName: 'Kotara',
        accountslist: [{
            balance: 5,
            currency: 'CHF'
        }] 
    };
    return {
       firstName: details.firstName,
       lastName: details.lastName,
        sayHello: function(){
            return this.firstName + ' ' + this.lastName + ' ' + details.accountslist[0].balance + ' ' + details.accountslist[0].currency;    
        }
    };
};
var person = personFactory();
console.log(person.sayHello());
