const init = (() => {
    class Account { 
        constructor (balance, currency, number){
            this.balance = balance;
            this.currency = currency;
            this.number = number;
        }
    };
    
    class Person {
        constructor (firstName, lastName, accountslist){
            this.firstName = firstName;
            this.lastName = lastName;
            this.accountslist = accountslist;
        }
        calculateBalance(){
            var sum=0;
            for( let stan of this.accountslist) {
                sum += stan.balance;
            }
            return sum;
        }
        addAcount(account){
            this.accountslist.push(account);
            function compareNr(a, b) {
                return a.number - b.number
            }
            this.accountslist.sort(compareNr);
        }
        filterPositiveAccounts(){
            return this.accountslist.filter(function(konto){
                return konto.balance > 0;
            });
        }
        findAccount(accountNumber){
            return this.accountslist.find(function(account){
                if(account.number === accountNumber){
                    return account;
                }
            });
        }
        withdraw(accountNumber, amount){      
            return  new Promise((resolve, reject) =>{
                let Konto = this.findAccount(accountNumber);
                if (Konto != undefined && amount <= Konto.balance){
                    Konto.balance -= amount;
                    setTimeout(function () {
                      return  resolve(accountNumber + ' ' + Konto.balance + ' ' + amount);
                    }
                    ,3000);
                }
                else{
                    return reject ('Brak takiego konta lub błąd salda!');  
                    };
            })
    
        }
        sayHello(){
            return this.firstName + ' ' + this.lastName + ' ' + this.calculateBalance();    
        }
    };

    const ja = new Person ('Krzysztof','Kotara',[new Account(10,'PLN',4)]);
    ja.addAcount(new Account(200,'EUR',2));
    ja.addAcount(new Account(30,'USD',1));
    ja.addAcount(new Account(10,'JPN',3));

    document.addEventListener("DOMContentLoaded", function () {
        let personNameBox = document.querySelector(".card-title");
        let personAccBox = document.querySelector(".card-text2");
        let ErrorBox = document.querySelector(".card-text3");
        let numberInput = document.querySelector("#number");
        let amountInput = document.querySelector("#amount");
        let withdrawButton = document.querySelector(".btn.btn-primary");
        function onClicked(){
            personNameBox.innerHTML = ja.firstName + ' ' + ja.lastName;
            let showAcc = function (nameOfAcc){
                var accList = '';
                for (let acc of nameOfAcc.accountslist){
                    accList = 'Numer konta:' + acc.number + ' Wartość:' + acc.balance + ' Waluta:' + acc.currency;
                    personAccBox.innerHTML += "<p>" + accList + "</p>" ;
                }
            };
            showAcc(ja);

            function withdrawal() {
                let accountNumber = parseInt(numberInput.value);
                let amount = parseInt(amountInput.value);

                function EmptyVal(){
                    numberInput.value = " ";
                    amountInput.value = " "; 
                }

                ja.withdraw(accountNumber, amount)
                    .then(() => {
                    ErrorBox.innerHTML = " ";
                    personAccBox.innerHTML = " ";
                    showAcc(ja);
                    EmptyVal();
                    })
                    .catch((failMessage) => {
                    console.log(failMessage);
                    ErrorBox.innerText = failMessage;
                    EmptyVal();
                    });
            } 

        withdrawButton.addEventListener("click", withdrawal); 
        }
        function onChenged() {
            withdrawButton.setAttribute('disabled','disabled');
            numberInput.addEventListener('change', inputChecker);
            amountInput.addEventListener('change', inputChecker);
            numberInput.addEventListener('keyup', inputChecker);
            amountInput.addEventListener('keyup', inputChecker);

            function inputChecker() {
                let accountNumber = numberInput.value;
                let amount = amountInput.value;

                
                    withdrawButton.disabled = !(accountNumber && amount);
               
            }
        }
    return {
    onClicked : onClicked(),
    onChenged : onChenged()
    }
   });
})();
